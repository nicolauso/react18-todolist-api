import { useEffect, useState } from "react";
import "./App.css";
import TodoList from "./components/TodoList";
import axios from "axios";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  // Utilisation du hook useEffect pour déclencher un événement au chargement de la page, ici déclencher la fonction getTodo qui récupère les data à l'API_URL
  useEffect(() => {
    getTodo();
  }, []);


  // Requête en méthode GET à l'API externe. Mise à jour du state todos avec la response.data et setTodos.
  async function getTodo() {
    try {
      const response = await axios.get(API_URL);
      setTodos(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
